# To build a Docker image

First, build a JAR, then run:

```
docker build . -t m2m.request
```
